package netns

import (
	"errors"
	"fmt"
	"github.com/vishvananda/netlink"
	"github.com/vishvananda/netns"
	"golang.org/x/sys/unix"
	"os"
	"runtime"
	"sync"
)

// NewNetns creates and returns named network namespace,
// or the current namespace if no name is specified
func NewNetns(name string) (netns.NsHandle, error) {
	// shortcut for current namespace
	if name == "" {
		return netns.Get()
	}

	// shortcut for existing namespace
	ns, err := netns.GetFromName(name)
	if err == nil {
		return ns, nil
	}

	if !errors.Is(err, os.ErrNotExist) {
		return 0, fmt.Errorf("unexpected error when getting netns handle %s: %s", name, err)
	}

	// do the dirty jobs in a locked os thread
	// go runtime will reap it instead of reuse it
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		runtime.LockOSThread()
		ns, err = netns.NewNamed(name)
	}()
	wg.Wait()
	return ns, err
}

// NewNetlink returns netlink handle created in the specified netns
func NewNetlink(name string) (netns.NsHandle, *netlink.Handle, error) {
	ns, err := NewNetns(name)
	if err != nil {
		return 0, nil, err
	}

	handle, err := netlink.NewHandleAt(ns, unix.NETLINK_ROUTE)
	return ns, handle, err
}
