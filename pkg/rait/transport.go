package rait

import (
	"encoding/base64"
	"fmt"
	"math"
	"net"
	"runtime"
	"strconv"
	"sync"

	"github.com/vishvananda/netlink"
	vnetns "github.com/vishvananda/netns"
	"gitlab.com/NickCao/RAIT/v4/pkg/entity"
	"gitlab.com/NickCao/RAIT/v4/pkg/misc"
	"gitlab.com/NickCao/RAIT/v4/pkg/netns"
	"go.uber.org/zap"
	"golang.org/x/sys/unix"
	"golang.zx2c4.com/wireguard/wgctrl"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

func (trans *Transport) PeerEnsure(r *RAIT, peer entity.Peer) ([]string, error) {
	sk, err := wgtypes.ParseKey(r.PrivateKey)
	if err != nil {
		return nil, err
	}

	if len(peer.PublicKey) != 32 {
		return nil, fmt.Errorf("wrong key length")
	}
	var pk wgtypes.Key
	copy(pk[:], peer.PublicKey)

	if sk.PublicKey().String() == pk.String() {
		return []string{}, nil
	}

	nsfd, handle, err := netns.NewNetlink(r.Namespace)
	if err != nil {
		return nil, err
	}
	defer handle.Delete()

	_, chandle, err := netns.NewNetlink(trans.Namespace)
	if err != nil {
		return nil, err
	}
	defer chandle.Delete()

	var links []string
	for i := range peer.Endpoints {
		ep := peer.Endpoints[i]
		if ep.AddressFamily != trans.AddressFamily {
			continue
		}
		name := trans.IFPrefix + strconv.Itoa(ep.SendPort)

		var ip net.IP
		addr, err := net.ResolveIPAddr(trans.AddressFamily, ep.Address)
		if err != nil || addr.IP == nil {
			zap.S().Infof("peer %s endpoint %s resolve failed in AF %s, falling back", base64.StdEncoding.EncodeToString(peer.PublicKey), ep.Address, trans.AddressFamily)
			switch ep.AddressFamily {
			case "ip4":
				ip = net.ParseIP("127.0.0.1")
			default:
				ip = net.ParseIP("::1")
			}
		} else {
			ip = addr.IP
		}

		config := wgtypes.Config{
			PrivateKey:   &sk,
			ListenPort:   &ep.SendPort,
			BindAddress:  net.ParseIP(trans.BindAddress),
			FirewallMark: &trans.FwMark,
			ReplacePeers: true,
			Peers: []wgtypes.PeerConfig{{
				PublicKey:                   pk,
				Endpoint:                    &net.UDPAddr{IP: ip, Port: trans.SendPort},
				PersistentKeepaliveInterval: nil,
				ReplaceAllowedIPs:           true,
				AllowedIPs:                  misc.IPNetAll,
			}},
		}

		if trans.RandomPort {
			config.ListenPort = nil
		}

		link, err := handle.LinkByName(trans.IFPrefix + strconv.Itoa(ep.SendPort))
		if err == nil {
			if link.Type() == "wireguard" {
				zap.S().Debugf("link %s already exists, skipping creation", name)
				goto created
			} else {
				zap.S().Infof("link %s already exists but is of wrong type, removing", name)
				goto removal
			}
		} else if _, ok := err.(netlink.LinkNotFoundError); !ok {
			return nil, fmt.Errorf("failed to get link %s: %s", name, err)
		} else {
			goto create
		}

	removal:
		err = handle.LinkDel(link)
		if err != nil {
			return nil, fmt.Errorf("failed to remove link %s: %s", name, err)
		}

	create:
		link = &netlink.Wireguard{LinkAttrs: netlink.LinkAttrs{
			Name:  name,
			MTU:   int(math.Min(float64(trans.MTU), float64(ep.MTU))),
			Group: uint32(trans.IFGroup)},
		}
		err = chandle.LinkAdd(link)
		if err != nil {
			return nil, fmt.Errorf("failed to create link %s: %s", name, err)
		}
		zap.S().Debugf("link %s created in transit namespace", name)

		err = chandle.LinkSetNsFd(link, int(nsfd))
		if err != nil {
			_ = chandle.LinkDel(link)
			return nil, fmt.Errorf("failed to move link %s: %s", name, err)
		}
		zap.S().Debugf("link %s moved into target namespace", name)

	created:
		err = handle.LinkSetUp(link)
		if err != nil {
			_ = handle.LinkDel(link)
			return nil, fmt.Errorf("failed to set up link %s: %s", name, err)
		}

		addrs, err := handle.AddrList(link, unix.AF_INET6)
		if err != nil {
			_ = handle.LinkDel(link)
			return nil, fmt.Errorf("failed to list addr on link %s: %s", name, err)
		}

		for _, addr := range addrs {
			if addr.IP.IsLinkLocalUnicast() {
				goto llfound
			}
		}

		err = handle.AddrAdd(link, misc.NewLLAddr())
		if err != nil {
			_ = handle.LinkDel(link)
			return nil, fmt.Errorf("failed to add addr to link %s: %s", name, err)
		}

	llfound:
		var waitGroup sync.WaitGroup
		waitGroup.Add(1)
		go func() {
			defer waitGroup.Done()
			runtime.LockOSThread()
			err = vnetns.Set(nsfd)
			if err != nil {
				err = fmt.Errorf("failed to move into netns: %s", err)
				return
			}

			var wg *wgctrl.Client
			wg, err = wgctrl.New()
			if err != nil {
				err = fmt.Errorf("failed to get wireguard control socket: %s", err)
				return
			}
			defer wg.Close()

			err = wg.ConfigureDevice(name, config)
			if err != nil {
				err = fmt.Errorf("failed to configure wireguard interface %s: %s", name, err)
				return
			}
		}()
		waitGroup.Wait()
		if err != nil {
			_ = handle.LinkDel(link)
			return nil, err
		}
		links = append(links, link.Attrs().Name)
	}
	return links, nil
}

func (trans *Transport) PeersEnsure(r *RAIT, peers []entity.Peer) ([]string, error) {
	var links []string
	mu := &sync.Mutex{}
	wg := &sync.WaitGroup{}
	for _, peer := range peers {
		wg.Add(1)
		go func(peer entity.Peer) {
			defer wg.Done()
			l, err := trans.PeerEnsure(r, peer)
			if err != nil {
				zap.S().Errorf("failed to setup peer %s: %s", base64.StdEncoding.EncodeToString(peer.PublicKey), err.Error())
			} else {
				mu.Lock()
				links = append(links, l...)
				mu.Unlock()
			}
		}(peer)
	}
	wg.Wait()

	_, handle, err := netns.NewNetlink(r.Namespace)
	if err != nil {
		return nil, err
	}
	defer handle.Delete()

	list, err := handle.LinkList()
	if err != nil {
		return nil, err
	}

	for _, l := range list {
		if l.Attrs().Group == uint32(trans.IFGroup) && l.Type() == "wireguard" {
			if !misc.StringIn(links, l.Attrs().Name) {
				err = handle.LinkDel(l)
				if err != nil {
					return nil, err
				}
			}
		}
	}
	return links, nil
}
